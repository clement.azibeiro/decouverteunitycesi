﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class Utilities
{
    public static int playerDeath = 0;

    public static string UpdateDeathCount(ref int countReference)
    {
        countReference += 1;
        return "La prochaine fois vous serez au numéro " + countReference;
    }

    public static void RestartLevel()
    {
        SceneManager.LoadScene(0);
        Time.timeScale = 1.0f;
        Debug.Log("Morts du joueur : " + playerDeath);
        UpdateDeathCount(ref playerDeath);
        Debug.Log("Morts du joueur : " + playerDeath);
    }

    public static bool RestartLevel(int screneIndex)
    {
        SceneManager.LoadScene(screneIndex);
        Time.timeScale = 1.0f;
        return true;
    }
}
