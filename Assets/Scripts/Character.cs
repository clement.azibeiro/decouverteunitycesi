﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character
{ 
    
    public string name;
    public int exp = 0;

    
    public Character()
    {
        name = "Non Assigné";
    }

    public Character(string name)
    {
        this.name = name;
    }

    
    public virtual void PrintStatsInfo()
    {
        Debug.LogFormat("Héro: {0} - {1} EXP", name, exp);
    }

    
    private void Reset()
    {
        this.name = "Non Assigné";
        this.exp = 0;
    }
}

public class Paladin: Character
{
    public Weapon weapon;
 
    public Paladin(string name, Weapon weapon): base(name)
    {
        this.weapon = weapon;
    }

    public override void PrintStatsInfo()
    {
        Debug.LogFormat("Salut {0} - prends ton arme {1}!", name, 
        weapon.name);
    }
}

public struct Weapon
{
    public string name;
    public int damage;

    public Weapon(string name, int damage)
    {
        this.name = name;
        this.damage = damage;
    }

    public void PrintWeaponStats()
    {
        Debug.LogFormat("Arme: {0} - {1} dommage(s)", name, damage);
    }
}
