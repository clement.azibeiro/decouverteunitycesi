﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    public float onScreenDealay = 3f;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(this.gameObject, onScreenDealay);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
