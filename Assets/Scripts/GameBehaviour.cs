﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using CustomExtensions;

public class GameBehaviour : MonoBehaviour, IManager
{
    private int _itemsCollected = 0;
    private int _playerHP = 1;

    public int maxItems = 3;
    public string labelText = "Collectionnez les objets et Gagnez votre liberté !";

    public bool showWinScreen = false;
    public bool showLooseScreen = false;

    private int _lives = 3;
    public int EnnemiLives
    {
        get { return _lives; }
        private set 
        { 
            _lives = value;
            if(_lives <= 0)
            {
                Destroy(this.gameObject);
                Debug.Log("Ennemi KO");
            }
        }
    }

    public int items
    {
        get { return _itemsCollected; }
        set 
        { 
            _itemsCollected = value;
           if(_itemsCollected >= maxItems)
            {
                labelText = "Vous avez trouvé tous les objets !";
                showWinScreen = true;
                Time.timeScale = 0f;
            }
           else
            {
                labelText = "Objet trouvé, il en reste encore " + (maxItems - _itemsCollected);
            }
        }
    }

    public int HP
    {
        get { return _playerHP; }
        set
        {
            _playerHP = value;
            if(_playerHP <= 0)
            {
                labelText = "Vous êtes KO";
                showLooseScreen = true;
                Time.timeScale = 0f;
            }
            else
            {
                labelText = "Ouille j'ai été touché !!!";
            }
        }
    }

    private string _state;
    public string State
    { 
        get { return _state; } 
        set 
        {
            _state = value;
        } 
    }

    // Start is called before the first frame update
    void Start()
    {
        Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnGUI()
    {
        GUI.Box(new Rect(20, 20, 150, 25), "Santé du joueur : " + _playerHP);
        GUI.Box(new Rect(20, 50, 150, 25), "Object collecté : " + _itemsCollected);
        GUI.Label(new Rect(Screen.width / 2 - 100, Screen.height - 50, 300, 50), labelText);

        if(showWinScreen)
        {
            if(GUI.Button(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 50, 200, 100), "Vous avez gagné !"))
            {
                Utilities.RestartLevel();
            }
        }

        if(showLooseScreen)
        {
            if (GUI.Button(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 50, 200, 100), "Vous avez perdu !"))
            {
                Utilities.RestartLevel();
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "Bullet(clone)")
        {
            EnnemiLives -= 1;
            Debug.Log("Coup critique");
        }
    }

    public void Initialize()
    {
        _state = "Manager installé ...";
        _state.DroleDeDebug();
        Debug.Log(_state);
    }
}
